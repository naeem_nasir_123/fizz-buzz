<?php
declare(strict_types=1);

namespace App\Tests\src\Services\Buzz;


use App\Services\Buzz\BuzzService;
use PHPUnit\Framework\TestCase;

/**
 * Class BuzzServiceTest
 *
 * @package App\Tests\src\Services\Buzz
 */
class BuzzServiceTest extends TestCase
{
    private BuzzService $buzzService;


    public function setUp(): void
    {
        parent::setUp();
        $this->buzzService = new BuzzService();
    }


    public function testIsConditionMatchesYes(): void
    {
        static::assertTrue($this->buzzService->isConditionMatches(10));
    }


    public function testIsConditionMatchesNo(): void
    {
        static::assertFalse($this->buzzService->isConditionMatches(7));
    }


    public function testGetLiteral(): void
    {
        static::assertEquals('Buzz', $this->buzzService->getLiteral());
    }

}