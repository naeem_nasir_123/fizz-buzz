<?php
declare(strict_types=1);


namespace App\Tests\src\Services\NumberListGenerator;


use App\Services\Buzz\BuzzService;
use App\Services\Fizz\FizzService;
use App\Services\FizzBuzz\FizzBuzzService;
use App\Services\NumberListGenerator\NumberListGeneratorService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class NumberListGeneratorServiceTest
 *
 * @package App\Tests\src\Services\NumberListGenerator
 */
class NumberListGeneratorServiceTest extends TestCase
{
    private MockObject|FizzService $fizzService;

    private MockObject|BuzzService $buzzService;

    private MockObject|FizzBuzzService $fizzBuzzService;

    private NumberListGeneratorService $actual;


    public function setUp(): void
    {
        parent::setUp();
        $this->fizzService     = $this->createMock(FizzService::class);
        $this->buzzService     = $this->createMock(BuzzService::class);
        $this->fizzBuzzService = $this->createMock(FizzBuzzService::class);
        $conditions            = [$this->fizzBuzzService, $this->buzzService, $this->fizzService];

        $this->actual = new NumberListGeneratorService($conditions);
    }


    public function testGenerateList(): void
    {
        $upperBound = 5;

        $this->fizzService->expects(static::atLeastOnce())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, true, false, false);

        $this->buzzService->expects(static::atLeastOnce())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, false, false, true);

        $this->fizzBuzzService->expects(static::atLeastOnce())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, false, false, false);

        $this->fizzService->expects(static::once())
            ->method('getLiteral')
            ->willReturn('fizz');

        $this->buzzService->expects(static::once())
            ->method('getLiteral')
            ->willReturn('buzz');

        $this->fizzBuzzService->expects(static::never())
            ->method('getLiteral');

        $expected = [
            0 => '1',
            1 => '2',
            2 => 'fizz',
            3 => '4',
            4 => 'buzz'
        ];

        static::assertSame($expected, $this->actual->generateList($upperBound));
    }


    public function testGenerateListWithAllFizz(): void
    {
        $upperBound = 5;

        $this->fizzService->expects(static::atLeastOnce())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(true, true, true, true, true);

        $this->buzzService->expects(static::atLeastOnce())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, false, false, false);

        $this->fizzBuzzService->expects(static::atLeastOnce())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, false, false, false);

        $this->fizzService->expects(static::exactly(5))
            ->method('getLiteral')
            ->willReturnOnConsecutiveCalls('fizz', 'fizz', 'fizz', 'fizz', 'fizz');

        $this->buzzService->expects(static::never())
            ->method('getLiteral');

        $this->fizzBuzzService->expects(static::never())
            ->method('getLiteral');

        $expected = [
            0 => 'fizz',
            1 => 'fizz',
            2 => 'fizz',
            3 => 'fizz',
            4 => 'fizz'
        ];

        static::assertSame($expected, $this->actual->generateList($upperBound));
    }


    public function testGenerateListWithAllFizzBuzz(): void
    {
        $upperBound = 5;

        $this->fizzService->expects(static::never())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, false, false, false);

        $this->buzzService->expects(static::never())
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(false, false, false, false, false);

        $this->fizzBuzzService->expects(static::exactly(5))
            ->method('isConditionMatches')
            ->willReturnOnConsecutiveCalls(true, true, true, true, true);

        $this->fizzService->expects(static::never())
            ->method('getLiteral');

        $this->buzzService->expects(static::never())
            ->method('getLiteral');

        $this->fizzBuzzService->expects(static::exactly(5))
            ->method('getLiteral')
            ->willReturnOnConsecutiveCalls('fizzBuzz', 'fizzBuzz', 'fizzBuzz', 'fizzBuzz', 'fizzBuzz');

        $expected = [
            0 => 'fizzBuzz',
            1 => 'fizzBuzz',
            2 => 'fizzBuzz',
            3 => 'fizzBuzz',
            4 => 'fizzBuzz'
        ];

        static::assertSame($expected, $this->actual->generateList($upperBound));

    }

}