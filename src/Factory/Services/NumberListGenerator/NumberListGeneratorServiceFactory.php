<?php
declare(strict_types=1);

namespace App\Factory\Services\NumberListGenerator;


use App\Services\Buzz\BuzzService;
use App\Services\ConditionInterface;
use App\Services\Fizz\FizzService;
use App\Services\FizzBuzz\FizzBuzzService;
use App\Services\NumberListGenerator\NumberListGeneratorService;
use Exception;

/**
 * Class NumberListGeneratorServiceFactory
 *
 * @package App\Factory\Services\NumberListGenerator
 */
class NumberListGeneratorServiceFactory
{

    /**
     * @param FizzService     $fizzService
     * @param BuzzService     $buzzService
     * @param FizzBuzzService $fizzBuzzService
     *
     * @return NumberListGeneratorService
     * @throws Exception
     */
    public function createService(
        FizzService $fizzService,
        BuzzService $buzzService,
        FizzBuzzService $fizzBuzzService
    ): NumberListGeneratorService
    {
        if (($fizzService instanceof ConditionInterface) === false) {
            throw new Exception(
                sprintf('Class: %s must implement ConditionInterface', FizzService::class)
            );
        }

        if (($buzzService instanceof ConditionInterface) === false) {
            throw new Exception(
                sprintf('Class: %s must implement ConditionInterface', BuzzService::class)
            );
        }

        if (($fizzBuzzService instanceof ConditionInterface) === false) {
            throw new Exception(
                sprintf('Class: %s must implement ConditionInterface', FizzBuzzService::class)
            );
        }

        return new NumberListGeneratorService(
            [
                $fizzBuzzService,
                $fizzService,
                $buzzService,
            ]
        );
    }

}