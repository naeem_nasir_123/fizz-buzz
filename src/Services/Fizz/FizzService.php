<?php

declare(strict_types = 1);

namespace App\Services\Fizz;


use App\Services\ConditionInterface;

/**
 * Class FizzService
 *
 * @package App\Services\Fizz
 */
class FizzService implements ConditionInterface
{
    const FIZZ_LITERAL = 'Fizz';


    /**
     * Return true if fizz condition matches
     *
     * @param int $number
     *
     * @return bool
     */
    public function isConditionMatches(int $number): bool
    {
        return $number % 3 === 0;
    }


    /**
     * @return string
     */
    public function getLiteral(): string
    {
        return self::FIZZ_LITERAL;
    }
}